from datetime import datetime
from functools import partial
from flask import Blueprint, jsonify, url_for, abort, request, g

from peewee import DoesNotExist

from ..extensions import db, auth
from ..models import User, Meal

DATE_FORMAT = '%d/%m/%Y - %H:%M'

api = Blueprint('api', __name__, url_prefix='/api/v1.0')


def can_access(same_user_id=None, all_roles=None):
    user = g.user

    if same_user_id:
        if user.id != same_user_id:
            return False

        # is the same user
        if all_roles is None:
            return True

    all_roles = set(all_roles)
    intersect = all_roles & user.roles
    if len(all_roles) == len(intersect):
        return True
    return False


def verify_or_401(*args):
    """If all are false will raise unauthorized()"""
    print(args)
    for pair in args:
        # Flasy value
        print(pair)
        if isinstance(pair, bool):
            if pair:
                return
            continue

        # only a function whiout arguments
        if callable(pair):
            if pair():
                return
            continue

        # call function and abort if False
        f, f_args = pair
        if f(*f_args):
            return
    unauthorized()


def get_serializer(fields):
    def get_fields(obj):
        return {field: getattr(obj, field) for field in fields}

    def f(obj):
        if isinstance(obj, db.Model):
            result = [get_fields(obj)]
        else:
            result = [get_fields(o) for o in obj]
        return jsonify({
            'objects': result
        })
    return f


def sanitizer(obj, required=None, optional=None):
    if not required:
        required = []
    if not optional:
        optional = []

    try:
        result = {field: obj[field] for field in required}
    except KeyError:
        abort(400)
    result.update({field: obj[field]
                   for field in optional if obj.get(field) is not None})
    return result

unauthorized = partial(abort, 401)
is_admin = partial(can_access, all_roles=[User.ADMIN])
is_self = partial(can_access, all_roles=[User.USER])

user_serializer = get_serializer(fields=['username', 'id', 'max_calories',
                                         'is_admin'])

serialize_meal = get_serializer(fields=['id',
                                        'user_id',
                                        'eaten_datetime',
                                        'calories',
                                        'content'])

# TODO: add sanitaze_edit_meal
sanitize_meal = partial(sanitizer, required=['calories', 'content'],
                        optional=['eaten_datetime'])
sanitize_edit_meal = partial(sanitizer, optional=['calories', 'content',
                                                  'eaten_datetime'])


@api.route('/token/')
@auth.required
def get_token():
    token = g.user.generate_auth_token()
    return jsonify({
        'id': g.user.id,
        'token': token.decode('ascii')
    })


@api.route('/users/', methods=['GET'])
@auth.required
def users():
    verify_or_401(is_admin())
    query = User.select(User.username, User.id,
                        User.max_calories, User.is_admin)
    return user_serializer(query)


@api.route('/users/<int:id>/', methods=['PUT'])
@auth.required
def edit_user(id):
    verify_or_401(is_self(id), is_admin)

    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        abort(404)
    password = request.json.get('password')
    max_calories = request.json.get('max_calories')

    # anything to change
    if not(max_calories or password):
        return '', 204

    if password:
        user.set_password(password)

    if max_calories:
        user.max_calories = max_calories

    user.save()
    return '', 204


@api.route('/users/<int:id>/', methods=['GET'])
@auth.required
def get_user(id):
    verify_or_401(is_self(id), is_admin)

    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        abort(404)
    return user_serializer(user)


@api.route('/users/', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    max_calories = request.json.get('max_calories')
    if username is None or password is None:
        abort(400)  # missing arguments

    try:
        user = User.get_it(username)
    except DoesNotExist:
        pass  # can creat it
    else:
        abort(400)  # existing user
    kwargs = {
        'username': username,
        'password': password
    }

    if max_calories:
        kwargs['max_calories'] = max_calories

    user = User.create(**kwargs)
    url = url_for('.get_user', id=user.id, _external=True)
    return user_serializer(user), 201, {'Location': url}


@api.route('/users/<int:id>/', methods=['DELETE'])
@auth.required
def delete_user(id):
    verify_or_401(is_self(id))

    try:
        user = User.get(User.id == id)
    except DoesNotExist:
        return '', 200

    user.delete_instance(recursive=False)
    return '', 200


@api.route('/users/<int:user_id>/meals/', methods=['GET'])
@auth.required
def meals(user_id):
    verify_or_401(is_self(user_id), is_admin)
    meals = Meal.select().where(Meal.user == user_id)\
        .order_by(Meal.eaten_datetime.desc())
    return serialize_meal(meals)


@api.route('/users/<int:user_id>/meals/<int:id>/', methods=['GET'])
@auth.required
def get_meal(user_id, id):
    verify_or_401(is_self(user_id), is_admin)

    try:
        meal = Meal.get_it(id=id, user_id=user_id)
    except DoesNotExist:
        abort(404)
    return serialize_meal(meal)


@api.route('/users/<int:user_id>/meals/', methods=['POST'])
@auth.required
def new_meal(user_id):
    verify_or_401(is_self(user_id))

    try:
        user = User.get(User.id == user_id)
    except DoesNotExist:
        abort(404)

    values = sanitize_meal(request.json)
    try:
        date = values['eaten_datetime']
    except KeyError:
        pass
    else:
        if not date:
            del values['eaten_datetime']
        else:
            values['eaten_datetime'] = datetime.strptime(date, DATE_FORMAT)

    meal = Meal.create(user=user, **values)
    url = url_for('.get_meal', user_id=meal.user_id, id=meal.id)
    return serialize_meal(meal), 201, {'Location': url}


@api.route('/users/<int:user_id>/meals/<int:id>/', methods=['PUT'])
@auth.required
def edit_meal(user_id, id):
    verify_or_401(is_self(user_id))
    values = sanitize_edit_meal(request.json)
    if not len(values):
        abort(400)

    try:
        date = values['eaten_datetime']
    except KeyError:
        pass
    else:
        values['eaten_datetime'] = datetime.strptime(date, '%d/%m/%Y - %H:%M')

    has_user = (Meal.user == user_id)
    has_meal = (Meal.id == id)
    update = Meal.update(**values).where(has_user & has_meal)
    if not update.execute():
        abort(404)  # 0 affected rows

    meal = Meal.get(Meal.id == id)
    return serialize_meal(meal)


@api.route('/users/<int:user_id>/meals/<int:id>/', methods=['DELETE'])
@auth.required
def delete_meal(user_id, id):
    verify_or_401(is_self(user_id))
    try:
        meal = Meal.get_it(id=id, user_id=user_id)
    except DoesNotExist:
        return '', 200
    meal.delete_instance()
    return '', 200
