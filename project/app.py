import os

from flask import Flask, render_template, g

from .extensions import db, auth, CustomJSONEncoder


def create_app(config=None, app_name='project', blueprints=None):
    static_folder = os.path.join(os.path.dirname(__file__), '..', 'static')
    app = Flask(app_name,
                static_folder=static_folder,
                template_folder="templates")

    app.json_encoder = CustomJSONEncoder
    app.config.from_object('project.config')
    app.config.from_pyfile('../local.cfg', silent=True)
    if config:
        app.config.from_pyfile(config)

    extensions_fabrics(app)
    blueprints_fabrics(app)
    # api_fabrics()
    # configure_logging(app)
    # error_pages(app)
    gvars(app)
    return app


def blueprints_fabrics(app):
    #  lueprints
    from .frontend import frontend
    from .api import api

    blueprints = (
        frontend,
        api,
    )

    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def extensions_fabrics(app):
    db.init_app(app)
    from .models import User, MODELS
    db.create_tables(MODELS, safe=True)
    # Init Auth
    app.User = User
    auth.init_app(app)


def api_fabrics():
    pass
    # initialize_api()


def error_pages(app):
    # HTTP error pages definitions
    return

    @app.errorhandler(403)
    def forbidden_page(error):
        return render_template("misc/403.html"), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("misc/404.html"), 404

    @app.errorhandler(405)
    def method_not_allowed(error):
        return render_template("misc/405.html"), 404

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("misc/500.html"), 500


def gvars(app):
    @app.before_request
    def gdebug():
        if app.debug:
            g.debug = True
        else:
            g.debug = False


def configure_logging(app):
    pass
