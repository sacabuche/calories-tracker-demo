from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from peewee import CharField, IntegerField, ForeignKeyField, DateTimeField
from peewee import DoesNotExist, BooleanField

from .extensions import db


DEFAULT__MAX_CALORIES = 2000
EXPIRE_TIME = 3600


class User(db.Model):
    username = CharField(max_length=100, unique=True)
    password_hash = CharField(max_length=128)
    max_calories = IntegerField(default=DEFAULT__MAX_CALORIES)
    is_admin = BooleanField(default=False)

    ADMIN = 'admin'
    USER = 'user'

    @property
    def roles(self):
        if self.is_admin:
            return {User.ADMIN, User.USER}
        return {User.USER}

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration=EXPIRE_TIME):
        print(expiration)
        s = Serializer(db.app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @classmethod
    def verify_auth_token(cls, token):
        s = Serializer(db.app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # expired
        except BadSignature:
            return None  # invalid

        try:
            return cls.get(cls.id == data['id'])
        except DoesNotExist:
            return None

    @classmethod
    def create(self, username, password, **kwargs):
        user = User(username=username, **kwargs)
        user.set_password(password)
        user.save()
        return user

    @classmethod
    def get_it(cls, username):
        return cls.get(cls.username == username)


class Meal(db.Model):
    user = ForeignKeyField(User, related_name='meals')
    eaten_datetime = DateTimeField(default=datetime.now)
    calories = IntegerField()
    content = CharField(max_length=140)

    @classmethod
    def get_it(cls, id, user_id):
        is_user = (Meal.user == user_id)
        is_meal = (Meal.id == id)
        return Meal.get(is_user & is_meal)


MODELS = [
    User,
    Meal,
]
