import calendar
from datetime import datetime

from flask import g, abort
from flask.json import JSONEncoder
from flask.ext.basicauth import BasicAuth
from peewee import DoesNotExist


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                if obj.utcoffset() is not None:
                    obj = obj - obj.utcoffset()
                millis = int(
                    calendar.timegm(obj.timetuple()) * 1000 +
                    obj.microsecond / 1000
                )
                return millis
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


class DbBasicAuth(BasicAuth):

    def init_app(self, app):
        self.app = app
        super(DbBasicAuth, self).init_app(app)

    def challenge(self):
        abort(401)

    def check_credentials(self, username_or_token, password):
        User = self.app.User
        user = User.verify_auth_token(username_or_token)
        if not user:
            try:
                user = User.get_it(username_or_token)
            except DoesNotExist:
                return False
            if not user.check_password(password):
                return False
        g.user = user
        return True


auth = DbBasicAuth()


from flask.ext.login import LoginManager

login_manager = LoginManager()
login_manager.login_view = 'auth.login'


from peewee import SqliteDatabase, Model


class ImproperlyConfigured(Exception):
    pass




class Database(SqliteDatabase):

    def __init__(self):
        pass

    def init_app(self, app):
        self.app = app
        path = app.config['DATABASE']
        if app.debug:
            path = app.config['DATABASE_DEBUG']
        super(Database, self).__init__(path)
        self.Model = self.get_model_class()

    def load_db(self):
        db_key = 'DATABASE'
        if self.app.debug:
            db_key = db_key + '_DEBUG'
        try:
            name = self.app.config[db_key]
        except KeyError:
            raise ImproperlyConfigured("Please specify %" % db_key)
        super(Database, self).__init__(name)
        self.register_handlers()

    def get_model_class(self):
        class CurrentModel(Model):
            class Meta:
                database = self

        return CurrentModel

    def safe_close(self, exc):
        if not self.is_closed():
            self.close()

    def register_handlers(self):
        self.app.before_request(self.connect)
        self.app.teardown_request(self.safe_close)


db = Database()
