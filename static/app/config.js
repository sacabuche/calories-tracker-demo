define(['knockout', 'plugins/http', 'lockr'], function(ko, http, Lockr) {
    var API = '/api/v1.0';
    var token = ko.observable(Lockr.get('token'));
    var user_id = ko.observable(Lockr.get('user_id'));
    var username = ko.observable(Lockr.get('username'));
    var tmp_username = ko.observable();
    var userData = ko.observable(Lockr.get('userData') || {});
    var viewMode = ko.computed(function () {
            return (userData() && userData().id) !== user_id();
    });

    var api = {
        token: make_api('token'),
        users: make_api('users'),
        user: make_api('users', user_id),
        meals: make_api('users', user_id, 'meals'),
        meal: make_api('users', user_id, 'meals', '$id')
    };

    var getAuthHeader = function (username, password) {
        if (!password) {
            password = 'unused';
        }
        if (!username) {
            username = token();
        }

        var auth = 'Basic ' + btoa(username + ':' + password);
        return {Authorization: auth};
    };

    function isFunction(x) {
        return Object.prototype.toString.call(x) == '[object Function]';
    }

    function make_api() {
        var path_list = arguments;

        var F = function () {
            var self = this;
            self.args = path_list;

            function getUri(replacements) {
                var template = [API];
                var value;
                for (var i in self.args) {
                    value = self.args[i];
                    if (value.indexOf && value.indexOf('$') === 0) {
                        value = replacements[value.slice(1, value.length)];
                    } else if (isFunction(value)) {
                        value = value();
                    }
                    template.push(value);
                }
                return template.join('/') + '/';
            }

            function make_caller(method) {
                return function (data, username, password) {
                    var uri = getUri(data);
                    headers = getAuthHeader(username, password);
                    return http[method](uri, data, headers);
                };
            }

            self.get = make_caller('get');
            self.post = make_caller('post');
            self.remove = make_caller('remove');
            self.put = make_caller('put');
        };
        return new F();
    }

    function update_token (user, password) {
        return api.token.get(null, user, password).then(
            function(d) {
                Lockr.set('token', d.token);
                Lockr.set('user_id', d.id);
                Lockr.set('username', username());
                token(d.token);
                user_id(d.id);
                tmp_username(false);
                return api.user.get();
            }).then(function (d) {
            var obj = d.objects[0];
            Lockr.set('userData', obj);
            console.log(obj);
            userData(obj);
        });
    }

    function rmInfo() {
        Lockr.flush();
        token(false);
        user_id(0);
    }

    return {
        tmp_username: tmp_username,
        username: username,
        user_id: user_id,
        user: userData,
        isLoged: token,
        api: api,
        rmInfo: rmInfo,
        update_token: update_token,
        viewMode: viewMode
    };
});
