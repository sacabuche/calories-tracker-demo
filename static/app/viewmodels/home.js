define(['durandal/app', 'durandal/system', 'knockout', 'plugins/http', 'moment'], function (app, system, ko, http, moment) {

    var C = require('config');

    var router = require('plugins/router');


    function fix(value) {
        if (value < 10){
            return '0' + value;
        }
        return value;
    }

    function Meal(o) {
        var self = this;
        self.id = ko.observable(o && o.id);
        self.content = ko.observable(o && o.content);
        self.calories = ko.observable(o && o.calories);

        var eaten = new Date(o && o.eaten_datetime);

        self.real_date =  o && moment.utc(o.eaten_datetime);

        self.day = ko.observable(o? eaten.getUTCDate(): null);
        self.month = ko.observable(o? eaten.getUTCMonth() + 1: null);
        self.year = ko.observable(o? eaten.getUTCFullYear(): null);
        self.hours = ko.observable(o? eaten.getUTCHours(): null);
        self.minutes = ko.observable(o? eaten.getUTCMinutes(): null);

        self.dateTime = ko.computed(function() {
            var day = self.day(),
                month = self.month(),
                year = self.year(),
                hours = self.hours(),
                minutes = self.minutes();

            if (!self.id()) {
                return '';
            }


            return [fix(day), '/', fix(month), '/', year, ' - ', fix(hours), ':', fix(minutes)].join('');
        });

        self.editing = ko.observable(!o);

        self.edit = function () {
            self.editing(true);
        };

        self.isVisible = ko.computed(function () {
            var id = self.id(),
                edit = self.editing(),
                viewMode = C.viewMode();

            if (viewMode) {
                return false;
            }

            if (!id) {
                return true;
            }

            return edit;
        });

        self.save = function () {
            var content = {
                content: self.content(),
                calories: self.calories(),
                eaten_datetime: self.dateTime(),
                id: self.id()
            };

            var promise;

            if (self.id()) {
                promise = C.api.meal.put(content);
            } else {
                promise = C.api.meals.post(content);
            }

            return promise.then(function (data) {
                updateMeals();
                self.editing(false);
            });
        };

        self.remove = function () {
            if (!self.id()) {
                return;
            }
            C.api.meal.remove({id: self.id()}).then(function () {
                updateMeals();
            });
        };
    }

    var allMeals = ko.observableArray([new Meal()]);

    function updateMeals() {
        return C.api.meals.get().then(function(result) {
            var remote_meals = result.objects;
            allMeals.removeAll();
            allMeals.push(new Meal());
            for (var i in remote_meals) {
                allMeals.push(new Meal(remote_meals[i]));
            }
        });
    }


    var ctor = function () {
        var self = this;
        self.user = C.user;
        self.filter = ko.observable({type: 'day', time_type:'all'});
        self.viewMode = C.viewMode;
        self.meals = ko.observable([]);
        self.username = ko.computed(function() {
            if (C.viewMode()) {
                return C.tmp_username();
            }
            return C.username();
        });

        var TIMES = {
            'breakfast': {begin:0, end:11},
            'lunch': {begin:11, end:17},
            'dinner': {begin:17, end:24}
        };


        self.set_time = function(name) {
            return function () {
                var obj = self.filter();
                obj.time_type = name;
                self.filter(obj);
            };
        };

        self.set_filter = function(name) {
            return function () {
                var obj = self.filter();
                obj.type = name;
                self.filter(obj);
            };
        };


        self.meals = ko.computed(function () {
            var filter = self.filter();
            var tmp = allMeals();
            var to_show;

            if (filter.type === 'all') {
                to_show = tmp;
            }

            if (filter.type === 'day' || filter.type === 'week' || filter.type === 'month' || filter.type === 'year') {
                filter.from = moment().startOf(filter.type);
                filter.to = moment().endOf(filter.type);
            }

            if (!tmp || !tmp[0]) {
                return [];
            }

            var meal;
            var i;
            if (!to_show) {
                to_show = [tmp[0]];
                for (i=1; i < tmp.length; i++) {
                    meal = tmp[i];
                    if ((meal.real_date > filter.from) && (meal.real_date < filter.to)){
                        to_show.push(meal);
                    }
                }
            }


            if (filter.time_type === 'all') {
                return to_show;
            }

            range = TIMES[filter.time_type];
            var time_show = [to_show[0]];
            for (i=1; i < to_show.length; i++) {
                meal = to_show[i];
                meal_time = meal.real_date.toObject();
                if ((meal_time.hours >= range.begin) && (meal_time.hours <= range.end)){
                    time_show.push(meal);
                }
            }


            return time_show;

        });


        self.sumCalories = ko.computed(function () {
            var result = 0;
            var tmp = self.meals();
            if (!tmp || !tmp.length) {
                return 0;
            }

            for (var i in tmp) {
                result += parseInt(tmp[i].calories(), 10) || 0;
            }
            return result;
        });

        self.isDanger = ko.computed(function () {
            var sumCalories = self.sumCalories();
            var user = self.user();
            var filter = self.filter();
            if ((filter.type !== 'day') || (filter.time_type != 'all')) {
                return false;
            }

            if (user.max_calories <= sumCalories) {
                return true;
            }

            return false;
        });


        updateMeals();
    };

    return ctor;
});