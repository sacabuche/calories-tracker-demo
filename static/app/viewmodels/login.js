define(['durandal/app', 'durandal/system', 'knockout', 'plugins/http'], function (app, system, ko, http) {
    var HOME = 'home';
    var C = require('config');

    var router = require('plugins/router');

    var ctor = function () {
        if (C.isLoged()) {
            router.navigate(HOME);
        }

        var self = this;
        self.allowAnonymus = true;
        self.username = C.username;
        self.password = ko.observable();
        self.canLogin = ko.observable(true);
        self.startLogin = function() {
            self.canLogin(false);
            C.update_token(self.username(), self.password()).then(function() {
                return router.navigate(HOME);
            }).fail(function () {
                app.showMessage('Verify your password/user', 'Login Error');
            })
            .always(function() {
                self.canLogin(true);
            });
        };
    };

    return ctor;
});