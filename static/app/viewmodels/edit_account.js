define(['durandal/app', 'durandal/system', 'knockout', 'plugins/http'], function (app, system, ko, http) {
    var HOME = 'home';

    var C = require('config');

    var router = require('plugins/router');

    var ctor = function () {
        var self = this;
        self.isSending = ko.observable(false);
        self.calories = ko.observable(C.user().max_calories);

        self.canSend = ko.computed(function (){
            var calories = parseInt(self.calories(), 10);
            if (!self.isSending() && calories) {
                return true;
            }

            return false;
        });

        self.save = function() {
            self.isSending(true);
            C.api.user.put({
                max_calories: self.calories()
            }).then(function() {
                return router.navigate('home');
            }).fail(function () {
                app.showMessage('Please put some number', 'Problem!');
            })
            .always(function() {
                self.isSending(false);
                C.update_token();
            });
        };
    };

    return ctor;
});