define(['plugins/router', 'config', 'knockout'], function (router, C, ko) {

    router.guardRoute = function (instance, instruction) {
        if (instance.allowAnonymus) {
            return true;
        }

        if (C.isLoged()) {
            return true;
        }

        return router.convertRouteToHash('login');

    };

    function logOut() {
        C.rmInfo();
        router.navigate('login');
    }

    return {
        isLoged: C.isLoged,
        router: router,
        logOut: logOut,
        homeUrl: ko.computed(function() {
            if (!C.isLoged()) {
                return router.convertRouteToHash('login');
            }
            return router.convertRouteToHash('home');

        }),
        activate: function () {
            return router.map([
                { route: ['', 'login'],                         moduleId: 'viewmodels/login',           title: 'Login',                 nav: false },
                { route: 'home',                                moduleId: 'viewmodels/home',            title: 'Home',                  nav: false },
                { route: 'new_account',                         moduleId: 'viewmodels/new_account',     title: 'New Account',           nav: false },
                { route: 'edit_account',                        moduleId: 'viewmodels/edit_account',    title: 'Edit Account',          nav: 1},
                { route: 'select_user',                         moduleId: 'viewmodels/select_user',     title: 'Select User',          nav: 2},
            ]).buildNavigationModel()
              .mapUnknownRoutes('viewmodels/login', 'not-found')
              .activate();
        }
    };
});