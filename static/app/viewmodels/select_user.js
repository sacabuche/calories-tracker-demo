define(['durandal/app', 'durandal/system', 'knockout', 'plugins/http'], function (app, system, ko, http) {

    var C = require('config');

    var router = require('plugins/router');
    var users = ko.observable([]);

    function setUsers() {
        C.api.users.get().then(function (data) {
            console.log(data.objects);
            users(data.objects);
        });
    }

    var ctor = function () {
        var self = this;
        self.users = users;
        self.user = C.user;

        if (!C.user().is_admin) {
            app.showMessage('You are not admin', 'Auth Error');
            router.navigate('home');
            return;
        }
        setUsers();

        self.select = function (x, v) {
            C.user_id(x.id);
            C.tmp_username(x.username);
            router.navigate('home');
        };
    };


    return ctor;
});