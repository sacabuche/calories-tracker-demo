define(['durandal/app', 'durandal/system', 'knockout', 'plugins/http'], function (app, system, ko, http) {
    var HOME = 'home';

    var C = require('config');

    var router = require('plugins/router');

    var ctor = function () {
        if (C.isLoged()) {
            router.navigate(HOME);
        }

        var self = this;
        self.allowAnonymus = true;
        self.isSending = ko.observable(false);
        self.username = ko.observable();
        self.password = ko.observable();
        self.password2 = ko.observable();
        self.is_password_ok = ko.computed(function() {
            var p = self.password(),
                p2 = self.password2();
            return p && (p === p2);
        });

        self.canSend = ko.computed(function (){
            var u = self.username(),
                not_sending = !self.isSending(),
                is_ok = self.is_password_ok();
            return u && not_sending && is_ok;
        });

        self.createUser = function() {
            self.isSending(true);
            C.api.users.post({
                username: self.username(),
                password: self.password()
            }).then(function() {
                app.showMessage('Super cool, now login', 'Cool');
                return router.navigate('login');
            }).fail(function () {
                app.showMessage('Please pick other username', 'Problem!');
            })
            .always(function() {
                self.isSending(false);
            });
        };
    };

    return ctor;
});